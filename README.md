```
 _____                        _     
|  ___|__  _ __ _ __ ___     | |___
| |_ / _ \| '__| '_ ` _ \ _  | / __|
|  _| (_) | |  | | | | | | |_| \__ \
|_|  \___/|_|  |_| |_| |_|\___/|___/
                                    
```

## 一、简介
对html5DOM中的Form表单对象进行重写，使其可以使用ajax无刷技术和服务器进行数据交换。
1.0 版本无任何依赖，直接引入JS文件即可使用。

### 1.语法和API说明

#### 语法说明：
```js
var formObj = new FormJs(option);
```
formObj : 表示对象名
FormJs : 表示类名
option : 表示传入一个对象

#### 例子：

```js
var formObj = new FormJs({
	formId : "demoForm",
	isRefresh : false,
	async : false,
	subButId : "submitButton", 
	onclick : function(){
	   	alert();
	},
	callback : function(state,data,xhr,ts,errorinfo){
		if ( state == "success" ){
			console.log(data);
		}
	}
});

```
#### option说明：
|option name | intro 
|---------|:---------
|``formId`` | form元素的ID : 类型为字符串
|``isRefresh`` | 是否采用无刷技术(如果设置为true，则会使用HTML5.0默认方式进行数据提交) : 类型为布尔值
|``async`` | 是否采用异步,类型为布尔值
|``subButId`` | form表单中作为提交事件触发的按钮的ID : 类型为字符串 
|``onclick`` | 请求前触发的回调函数
|``callback`` | 提交后的回调函数，包含以下五个参数：|

1. state : 表示状态,字符串类型,包含（success，complete，error）分别代表（成功，完成，出错）
1. data : 请求成功后服务器返回的数据，强制类型为字符串。
1. xhr : 返回xhr对象
1. ts: 返回xhr的状态
1. errorinfo : 如果出现错误，则返回的错误信息