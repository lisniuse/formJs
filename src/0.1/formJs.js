Class.$("FormObj",{
	_constructor : function (option) {  
		if (typeof($)=="undefined"){throw new Error("Please confirm whether you have tried the Ajax library.");return undefined;}
		if (!('ajax' in $)){throw new Error("Please confirm whether you have tried the Ajax library.");return undefined;}
		var me = this; 
		me.option = option;
		me.formId = me.option.formId;
		me.formObj = document.getElementById(me.formId);
		me.subButObj = document.getElementById(me.option.subButId);
		me.tFormSubObj = me.formObj.childNodes;
		me._getAllSubTag();
		me._listenOnClick();
		if ( me.option.isRefresh == false){ 
			me.formObj.onsubmit = function () {
			   return false
			};
		}
	},
	_getAllSubTag : function(){
		var me = this;
		me.formTags = [];
		for (var i = 0; i < me.tFormSubObj.length; i++) {
			if (me.tFormSubObj[i].name){
				me.formTags.push(me.tFormSubObj[i]);
			}
		}
	},
	_getJsonForTags : function(){
		var me = this;
		me.dataJson = {};
		for (var i = 0; i < me.formTags.length; i++) { 
			me.dataJson[me.formTags[i].name] = me.formTags[i].value;
		}
	},
	_listenOnClick : function(){
		var me = this;
		me.subButObj.onclick = function(){ 
            me.option.onclick();
			me._getJsonForTags();
			me._sendData();
		};
	},
	_sendData : function(){
		var me = this;
		$.ajax({
			url : me.formObj.action,
			type : me.formObj.method.toUpperCase(),
			data : me.dataJson,
			async : me.option.async,
			cache : false,
			dataType : 'text',
			success : function(data, ts){
				var xhr = null;
				var errorinfo = null;
				me.option.callback("success",data,xhr,ts,errorinfo);
			},
			complete : function(xhr, ts){
				var data = null;
				var errorinfo = null;
				me.option.callback("complete",data,xhr,ts,errorinfo);
			},
			error : function(xhr, ts, errorinfo){
				var data = null;
				me.option.callback("error",data,xhr,ts,errorinfo);
			}
		});
	}
});